import { Component, OnInit } from '@angular/core';
import {BallsComponent} from '../balls/balls.component';
import {RobervalService} from '../roberval.service';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {

  constructor(private robervalService: RobervalService) { }

  findTheHeaviest(){
    this.robervalService.findTheHeaviestBallPost().subscribe((res) => {
      var ball = JSON.parse(res.text());  // If response is a JSON use json()
      
      this.robervalService.setResult(ball)
    });
  //BallsComponent.balls;
  }

  hasUserPickedAball(){
    return this.robervalService.getPickedBallId() !== null;

  }
  getResultId(){
    return this.robervalService.getResult().id;
  }
  
  resetBalls(){
    this.robervalService.resetBalls();
  }
  ngOnInit() {
  }

}
