import { Component, OnInit } from '@angular/core';
import { Ball } from '../ball'
import {RobervalService} from '../roberval.service';

@Component({
  selector: 'app-balls',
  templateUrl: './balls.component.html',
  styleUrls: ['./balls.component.css']
})
export class BallsComponent implements OnInit {
  //init ball array
  balls : Ball[] = [];
  pickedBall : Ball;
  imageStyle: string;
  constructor(private robervalService : RobervalService) { 
  }
pickBall(ballId){
    this.resetBalls();
    this.pickedBall = this.balls.filter(b => b.id == ballId)[0];
    this.pickedBall.picked = true;
    this.pickedBall.weight = 20;
    this.robervalService.setPickedBall(this.pickedBall);
    console.log(this.balls);
    console.log("picked :");
    console.log(this.pickedBall);
    this.robervalService.setBalls(this.balls);

  }
  
  resetBalls(){
    this.balls = [];
    for(let i =1 ; i<= 8; i++){
      let ball : Ball = {
        id:i,
        weight:10,
        picked:false
      };
      this.balls.push(ball)
    }
  }
  getImageStyle(ball) {
      return ball.id === this.robervalService.getPickedBallId() ? 'image-picked' : 'image';
  }
  ngOnInit() {
    this.resetBalls();
  }

}
