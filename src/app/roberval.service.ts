import { Injectable } from '@angular/core';
import {Http, Headers,RequestOptions} from '@angular/http';
import { Ball } from './ball'

@Injectable({
  providedIn: 'root'
})
export class RobervalService {
  balls : Ball[] ;
  result :Ball;
  pickedBall :Ball;

  constructor(private http:Http) {
  }
  
  public resetBalls(){
    this.pickedBall = null;
    this.result = null;
    this.balls = [];
    for(let i =1 ; i<= 8; i++){
      let ball : Ball = {
        id:i,
        weight:10,
        picked:false
      };
      this.balls.push(ball)
    }
  }
  public setResult(ball){
    this.result = ball;
  }
  public getResult(){
    return this.result;
  }
  public setBalls(balls){
    this.balls = balls;
  }
  public setPickedBall(ball){
    this.pickedBall = ball;
  }
  public getPickedBallId(){
    return this.pickedBall ? this.pickedBall.id : null;
  } 
  public getImageStyle(ball) {
    return ball.picked ? 'image-picked' : 'image';
  }

  /**
   * This does the backend API call
   */
  public findTheHeaviestBallPost() {
    let headers = new Headers({ 'Content-Type': 'application/json','Access-Control-Allow-Origin': '*' });
    let options = new RequestOptions({ headers: headers });
    if(this.balls !== undefined && this.balls.length !== 0){
        return this.http.post(
          "http://localhost:8081/findHeaviestBall",
          this.balls,options)
      }
    }

}
