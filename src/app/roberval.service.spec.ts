import { TestBed, inject } from '@angular/core/testing';

import { RobervalService } from './roberval.service';

describe('RobervalService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RobervalService]
    });
  });

  it('should be created', inject([RobervalService], (service: RobervalService) => {
    expect(service).toBeTruthy();
  }));
});
