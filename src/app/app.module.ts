import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BallsComponent } from './balls/balls.component';
import { BalanceComponent } from './balance/balance.component';
import { RobervalService } from './roberval.service';
import { HttpModule } from '@angular/http';
@NgModule({
  declarations: [
    AppComponent,
    BallsComponent,
    BalanceComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [RobervalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
