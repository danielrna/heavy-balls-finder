var express = require('express');
var app = express();
var bodyParser = require('body-parser')
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.all('*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.post('/findHeaviestBall', function (req, res) {
    var balls = req.body;
    res.send(findHeaviest(balls));

})

findHeaviest = function(balls){
  //le but est de comparer successivement la première balle aux autres pour trouver la plus lourde
  var heaviest = balls[0];
  for(i =1; i<=balls.length;){
      //premier cas : la première balle est la plus lourde
      if(heaviest.weight > balls[i].weight){
        return heaviest;
      }
      //si la balle suivante est plus lourde que celle d'avant, on a trouvé la plus lourde
      if(balls[i] !== undefined && heaviest.weight < balls[i].weight){
        heaviest=balls[i];
        return heaviest;
      }
      //sinon on la compare à celle d'apres
      else{
        i++;
      }
  };
}
var server = app.listen(8081, function () {

  var host = server.address().address
  var port = server.address().port
  console.log("Example app listening at http://%s:%s", host, port)

})